# -*- coding: utf -*-
# Copyright (2021) Cardiff University (macleoddm@cardiff.ac.uk)

"""Dummy project to test infrastructure
"""

__author__ = "Duncan Macleod <duncan.macleod@ligo.org>"
__license__ = "MIT"
__version__ = "0.0.8"

import sys


def dummy():
    print("Hello world")


if __name__ == "__main__":
    sys.exit(dummy())
